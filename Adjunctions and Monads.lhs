\documentclass{article}
%include polycode.fmt
\usepackage{tikz-cd}
\usepackage{graphicx}
\usepackage{amsmath, amssymb}
\usepackage{amsthm,thmtools}
\usepackage{hyperref, cleveref}
\usepackage[most]{tcolorbox}
\usepackage[utf8]{inputenc}
\newcommand{\dotarrow}{% to be used in math mode...
	\mathrel{\ooalign{\hss\raise.65ex\hbox{\scalebox{1.25}{\normalfont .}}%
	\kern0.35ex\hss\cr$\longrightarrow$}}}
		
\declaretheorem[name=Lemma]{lemma}
\declaretheorem[name=Theorem]{theorem}
\declaretheorem[name=Underspecified theorem]{theoremish}

\declaretheoremstyle[postheadspace=\newline]{def}
\declaretheorem[style=def,name=Definition]{definition}

%opening
\title{Adjunctions and Monads \\
      \large A Love Story}
\author{Cristóvão Gomes Ferreira}

\begin{document}

\newcommand{\blackqed}{
	\renewcommand{\qedsymbol}{$\blacksquare$}
	\qed
	\renewcommand{\qedsymbol}{$\square$}
}

\newcommand{\ignore}[1]{}
\newcommand\mdoubleplus{\mathbin{+\mkern-10mu+}}

\newcommand{\catname}[1]{{\normalfont\textbf{#1}}}


\newtcolorbox[auto counter%, number within=chapter, 
    %number freestyle={\noexpand\thechapter.\noexpand\arabic{\tcbcounter}}
    ]
    {mylemma}[2][]{
    enhanced,
    breakable,
    fonttitle=\bfseries,
    title=Lemma~\thetcbcounter: #2,    
    #1
}

\maketitle

\begin{abstract}
We are going to prove that each adjunction defines a monad, thus entering the first part of our Monad and Adjunction love story!
\end{abstract}

\section{Definitions}

\subsection{Prelude}

\ignore {
\begin{code}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleInstances #-}
module AdjunctionDefinitions where
import CategoryPrelude
import Data.Foldable (Foldable(..))
\end{code}
}

For these notes, we define the following conventions:

\begin{itemize}
	\item $\mathcal{C}$, $\mathcal{D}$ or any calligraphed letter is a variable denoting a category.
  \item $\catname{Hask}, \catname{Set}$ or any bold text denotes a category.
	\item $Hom(x,y)$ is the set of arrows between objects $x$ and $y$.
	\item $I$ is the identity functor ($Ic=c$, $If=f$).
	\item Basic category theory identities are trivial, i.e., will not be invoked but just used.
  \item $U$ is the forgetful functor.
  \item $\catname{Set}$ and $\catname{Hask}$ are essentially the same category for our purposes\footnote{In these notes we mostly use the fact that $\catname{Hask}$ is cartesian closed, and $\catname{Set}$ is cartesian closed. We also use the \catname{Mon} and \catname{Semigroup} subcategories, but those are also defined in both \catname{Hask} and \catname{Set}.}. We denote both by $\catname{Hask}$.
  \item In \catname{Hask} and \catname{Set}, lambda defined functions ($\lambda x . term$) are internal functions (i.e.: "elements" of $a \rightarrow b$, which is an object), extensionably defined functions ($f\ x = term$) are arrows of the underlying category. These are more or less the same, but not quite.
\end{itemize}

\subsection{Adjunction}

Let $F$ and $G$ such that:

\begin{tikzcd}
  \mathcal{C} \arrow[r, shift left, "F"] & \mathcal{D} \arrow[l, shift left, "G"]
\end{tikzcd}

We say that $F$ is left adjoint to $G$, written $F \dashv G$, when some rules apply.

There are several equivalent definitions of the rules in this relation, in these notes we are going to use the following:
	
$F \dashv G$ if there are two natural transformations
$\eta \colon I \dotarrow GF$ and $\epsilon \colon FG \dotarrow I$ such that the following diagrams commute:

\begin{tikzcd}
 & G F G a \arrow[rd, "G\epsilon_a"] \\
 G a \arrow[ru, "\eta_{G a}"] \arrow[rr, equal,"id_{G a}"] & & G a
\end{tikzcd}
\begin{tikzcd}
 & F G F a \arrow[rd, "\epsilon_{F a}"] \\
 F a \arrow[ru, "F\eta_a"] \arrow[rr, equal, "id_{F a}"] & & F a
\end{tikzcd}

\begin{tcolorbox}[breakable] 
In Haskell, this typeclass would be:

\begin{code}
class (Functor f c d, Functor g d c)
  => Adjunction f g c d where
  eta :: c x (g (f x))
  epsilon :: d (f (g y)) y
\end{code}

With rules:

\begin{itemize}
  \item \texttt{eta} is a natural transformation
  \item \texttt{epsilon} is a natural transformation
  \item | map epsilon . eta = id |
  \item | epsilon . map eta = id |
\end{itemize}
\end{tcolorbox}

\subsection{Monad}

A Monad is a functor $M \colon \mathcal{C} \rightarrow \mathcal{C}$, and two natural transformations $\eta \colon I \dotarrow M$, $\mu \colon M^2 \dotarrow M$.
Such that the following diagrams commute:

\begin{tikzcd}
  M a \arrow[r, "\eta_{M a}"] \arrow[rd, swap, equal, "id_{M a}"] & M^2 a \arrow[d, "\mu_a"] & M a \arrow[l, swap, "M \eta_a"] \arrow[ld, equal, "id_{M a}"] \\
  & M a
\end{tikzcd}
\begin{tikzcd}
 M^3 a \arrow[r, "M\mu_a"] \arrow[d, "\mu_{M a}"] & M^2 a \arrow[d, "\mu_a"] \\
 M^2 a \arrow[r, "\mu_a"] & M a
\end{tikzcd}

\begin{tcolorbox}[breakable] 
In Haskell, this typeclass would be:

\begin{code}
class (Functor m c c)
  => Monad m c where
  eta :: c x (m x)
  bind :: c a (m b) -> c (m a) (m b)
  bind f = mu . fmap f
  mu :: c (m (m x)) (m x)
  mu = bind id
\end{code}

With rules:

\begin{itemize}
  \item \texttt{eta} is a natural transformation
  \item \texttt{bind} is a natural transformation
  \item | bind eta = id |
  \item | bind f . eta = f |
  \item | bind g . bind f = bind (bind g . f) |
\end{itemize}
\end{tcolorbox}

\section{Main section}

\subsection{Motivation}

Let us go through a list of known adjunctions. The proof that these are adjunctions is left as an exercise to the reader:

\begin{itemize}
  \item
    \begin{tikzcd}
      \catname{Hask} \arrow[r, shift left, "I"] &[10pt] \catname{Hask} \arrow[l, shift left, "I"]
    \end{tikzcd}, with $\eta_a = id_a$ and $\epsilon_a = id_a$;
  \item
    \begin{tikzcd}
      \catname{Hask} \arrow[r, shift left, "\lbrack - \rbrack"] &[10pt] \catname{Mon} \arrow[l, shift left, "U"]
    \end{tikzcd}, with $\eta_a = |singleton|_a$ and $\epsilon_m = |mconcat|_m$;
  \item
    \begin{tikzcd}
      \catname{Hask} \arrow[r, shift left, "NonEmpty"] &[10pt] \catname{Semigroup} \arrow[l, shift left, "U"]
    \end{tikzcd}, with $\eta_a = |singleton|_a$ and $\epsilon_s = |sconcat|_s$;
  \item
    \begin{tikzcd}
      \catname{Hask} \arrow[r, shift left, "{(Maybe -, Nothing)}"] &[10pt] \catname{Hask}_{\star} \arrow[l, shift left, "U"]
    \end{tikzcd}, with $\eta_a = |Just|_a$ and $\epsilon_{a, x |::| a} = |fromMaybe|_a x$;
  \item
    \begin{tikzcd}
      \catname{Hask} \arrow[r, shift left, "{(a, -)}"] &[10pt] \catname{Hask} \arrow[l, shift left, "a \rightarrow -"]
    \end{tikzcd}, with $\eta_a\ x = \lambda a . (a,x) $ and $\epsilon_a\ (a,f) = f\ a$.
\end{itemize}

One pattern emerges: for all these adjunctions, $GF$ is a known Monad in \catname{Hask}!

\begin{itemize}
  \item
    $II$ = $I$ is the |Identity| monad;
  \item
    $U\ \lbrack - \rbrack$ is the |[]| monad;
  \item
    $U\ NonEmpty$ is the |NonEmpty| monad;
  \item
    $U\ (Maybe -, Nothing)$ is the |Maybe| monad;
  \item
    $a \rightarrow (a, -)$ is the |State| monad.
\end{itemize}

More that that! $\eta_a$ is exactly |pure| and $\epsilon_a$ is either literally |join| or something close!

We wonder if this could be generally true: Can we define $GF$ a monad for any adjunction?

Luckily, the answer is yes, and the proof is just in the next section! How convenient!

\subsection{Main section}

We do not know yet the exact details, but our main result should look something like this:

\begin{theoremish}
  Given any adjunction $F \dashv G$, with unit $\eta$ and counit $\epsilon$, then $M=GF$ is a monad, for some sensible $\eta$ and $\mu$.
\end{theoremish}

But what are these sensible $\eta$ and $\mu$?

Well, the adjunction's $\eta \colon I \dotarrow GF$ already has the correct domain and codomain, so we will just take it as our monad's $\eta$.

A bit more involved is defining $\mu$, but by realizing that $\mu \colon M^2=G\boldsymbol{FG}F \dotarrow GF$ while $\epsilon \colon \boldsymbol{FG} \dotarrow I$, we tentatively define:
\[ \mu_a = G \epsilon_{F a} \]

We are now ready for stating our theorem, and it is as follows:


\begin{theorem}
  Given any adjunction $F \dashv G$, with unit $\eta$ and counit $\epsilon$, then $M=GF$ is a monad, with $\eta=\eta$ and $\mu=G\epsilon_{F}$.
\end{theorem}


\begin{proof}

We have to prove:

\begin{enumerate}
  \item \label{etanattrans} $\eta$ is a natural transformation;
  \item \label{munattrans} $\mu = G\epsilon_F$ is a natural transformation;
  \item \label{diagrams} The identity diagrams of a monad commute.
\end{enumerate}

These first two items do not use the adjunction and monad identities, and as such we will not detail them in this section. In fact:

\begin{itemize}
  \item \Cref{etanattrans} is given by the fact that the original eta is a natural transformation
  \item \Cref{munattrans} is a corollary of two easy lemmas proven in \cref{lemmas}.
\end{itemize}

We focus, then, in \cref{diagrams}.

Replacing $\mu$ with our definition in the monad identity diagrams, we need to prove:

\[
\begin{tikzcd}
  GF a \arrow[r, "\eta_{GF a}"] \arrow[rd, swap, equal, "id_{GF a}"] & GFGF a \arrow[d, "G\epsilon_{Fa}"] & GF a \arrow[l, swap, "GF \eta_a"] \arrow[ld, equal, "id_{M a}"] \\
  & GF a
\end{tikzcd}
\begin{tikzcd}
 GFGFGF a \arrow[r, "GFG\epsilon_{Fa}"] \arrow[d, "G\epsilon_{FGF a}"] & GFGF a \arrow[d, "G\epsilon_{Fa}"] \\
 GFGF a \arrow[r, "G\epsilon_{Fa}"] & GF a
\end{tikzcd}
\]

The identities on the left are easy to prove: $G\epsilon_{Fa} \circ \eta_{GF a} = id_{GF a}$, by the first adjunction identity, and $G\epsilon_{Fa} \circ GF\eta_a = G (\epsilon_{Fa} \circ F\eta_a) = G (id_{Fa}) = id_{GFa}$, by the second adjunction identity (and some functor laws).

The identities on the right are a $90^{\circ}$ rotation of the naturality square of $G\epsilon$ for $\epsilon_{Fa}$\footnote{If you are wondering why this argument does not hold in general for any $\mu$, note that the diagram would require $\mu = M \mu'$ or $\mu = \mu'_M$ with $\mu' \colon M \dotarrow I$}:

\[
\begin{tikzcd}
  GFGFGF a \arrow[r, "G\epsilon_{FGFa}"] \arrow[d, "GFG\epsilon_{Fa}"] &[20pt] GFGF a \arrow[d, "G\epsilon_{Fa}"] \\
  GFGF   a \arrow[r, "G\epsilon_{Fa}"  ]                               &       GF   a
\end{tikzcd}
\]

Given that $G\epsilon$ is a natural transformation, as proved in \cref{funcnat}, our proof is finished.

\end{proof}

\section{Lemmas}\label{lemmas}

\subsection{Functor image of natural transformation is a natural transformation}
Given functors $F_1, F_2 \colon \mathcal{C} \rightarrow \mathcal{D}$ and $G \colon \mathcal{D} \rightarrow \mathcal{E}$, and a natural transformation $\alpha \colon F_1 \dotarrow F_2$, then:
\begin{lemma}[label=funcnat]
  The function bundle $G\alpha$ given by $(G\alpha)_a = G (\alpha_a) : G F_1 a \rightarrow G F_2 a $ is a natural transformation $\colon GF_1 \dotarrow GF_2$.
\end{lemma}

\begin{proof}
What we need to prove is just that the natural square

\[
\begin{tikzcd}
GF_1 a \arrow[r, "G \alpha_a"] \arrow[d, "GF_1 f"] & GF_2 a \arrow[d, "GF_2 f"] \\
GF_1 b \arrow[r, "G \alpha_b"] & GF_2 b
\end{tikzcd}
\]

commutes, i.e.: $GF_2 f \circ G \alpha_a = G \alpha_b \circ GF_1 f$.

This is not hard:
\[
\begin{aligned}
  GF_2 f \circ G \alpha_a =
  \\ G (F_2 f \circ \alpha_a) = && \text{(by naturality of $\alpha$)}
  \\ G (\alpha_b \circ F_1 f) =
  \\ G \alpha_b \circ GF_1 f
\end{aligned}
\]

\end{proof}

\subsection{Natural transformation restricted to the image of a functor is a natural transformation}
Given functors $G_1, G_2 \colon \mathcal{C} \rightarrow \mathcal{D}$ and $F \colon \mathcal{B} \rightarrow \mathcal{C}$, and a natural transformation $\alpha \colon G_1 \dotarrow G_2$, then:
\begin{lemma}[label=natfunc]
{The function bundle $\alpha_F$ given by $(\alpha_F)_a = \alpha_{F a} : G_1 F a \rightarrow G_2 F a $ is a natural transformation $\colon G_1F \dotarrow G_2F$.}
\end{lemma}

\begin{proof}
What we need to prove is just that the natural square

\[
\begin{tikzcd}
G_1F a \arrow[r, "\alpha_{Fa}"] \arrow[d, "G_1F f"] & G_2F a \arrow[d, "G_2F f"] \\
G_2F b \arrow[r, "\alpha_{Fb}"] & G_2F b
\end{tikzcd}
\]

commutes. But this is just the natural transformation square for $\alpha$ at specific objects, so we're done.

\end{proof}

\end{document}


