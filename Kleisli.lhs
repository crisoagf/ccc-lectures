\documentclass{article}
%include polycode.fmt
\usepackage{tikz-cd}
\usepackage{graphicx}
\usepackage{amsmath, amssymb}
\usepackage{amsthm,thmtools}
\usepackage{hyperref, cleveref}
\usepackage[most]{tcolorbox}
\usepackage[utf8]{inputenc}
\newcommand{\dotarrow}{% to be used in math mode...
	\mathrel{\ooalign{\hss\raise.65ex\hbox{\scalebox{1.25}{\normalfont .}}%
	\kern0.35ex\hss\cr$\longrightarrow$}}}
		
\declaretheorem[name=Lemma]{lemma}
\declaretheorem[name=Theorem]{theorem}
\declaretheorem[name=Underspecified theorem]{theoremish}

\declaretheoremstyle[postheadspace=\newline]{def}
\declaretheorem[style=def,name=Definition]{definition}

%opening
\title{Adjunctions and Monads \\
      \large A Love Story -- Part III}
\author{Cristóvão Gomes Ferreira}

\begin{document}

\newcommand{\blackqed}{
	\renewcommand{\qedsymbol}{$\blacksquare$}
	\qed
	\renewcommand{\qedsymbol}{$\square$}
}

\newcommand{\ignore}[1]{}
\newcommand\mdoubleplus{\mathbin{+\mkern-10mu+}}

\newcommand{\catname}[1]{{\normalfont\textbf{#1}}}
\newcommand{\algebra}[3]{\begin{tikzcd}#1 #2 \arrow[d,"#3"] \\ #2\end{tikzcd}}
\newcommand{\algebralike}[3]{\begin{tikzcd}#1 \arrow[d,"#3"] \\ #2\end{tikzcd}}
\newcommand{\Kleisli}[1]{\catname{Kleisli(#1)}}
\newcommand{\Kleislicomp}[1]{\diamond}
\newcommand{\Kleislicompdef}[4]{\mu_{#4} \circ #1 #2 \circ #3}
\newcommand{\Fdef}[2]{\eta_{#2} \circ #1}
\newcommand{\Gdef}[3]{\mu_{#3} \circ #1 #2}

\maketitle

\begin{abstract}
We are going to prove that given a monad $M$, we can define yet another adjunction $F \dashv G$, such that $GF = M$, for the final part of our Monad and Adjunction love story!
\end{abstract}

\section{Definitions}

\subsection{Prelude}

\ignore {
\begin{code}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleInstances #-}
module Kleisli where
import CategoryPrelude
import Data.Foldable (Foldable(..))
\end{code}
}

For these notes, we define the following conventions:

\begin{itemize}
	\item $\mathcal{C}$, $\mathcal{D}$ or any calligraphed letter is a variable denoting a category.
  \item $\catname{Hask}, \catname{Set}$ or any bold text denotes a category.
	\item $Hom(x,y)$ is the set of arrows between objects $x$ and $y$.
	\item $I$ is the identity functor ($Ic=c$, $If=f$).
	\item Basic category theory identities are trivial, i.e., will not be invoked but just used.
  \item When a commutative diagram is invoked in an equation, the leftmost path will be the right-hand side of the equation and the rightmost path will be the left-hand side.
\end{itemize}

\subsection{Adjunction}

Let $F$ and $G$ such that:

\begin{tikzcd}
  \mathcal{C} \arrow[r, shift left, "F"] & \mathcal{D} \arrow[l, shift left, "G"]
\end{tikzcd}

We say that $F$ is left adjoint to $G$, written $F \dashv G$, when some rules apply.

There are several equivalent definitions of the rules in this relation, in these notes we are going to use the following:
	
$F \dashv G$ if there are two natural transformations
$\eta \colon I \dotarrow GF$ and $\epsilon \colon FG \dotarrow I$ such that the following diagrams commute:

\begin{tikzcd}
 & G F G a \arrow[rd, "G\epsilon_a"] \\
 G a \arrow[ru, "\eta_{G a}"] \arrow[rr, equal,"id_{G a}"] & & G a
\end{tikzcd}
\begin{tikzcd}
 & F G F a \arrow[rd, "\epsilon_{F a}"] \\
 F a \arrow[ru, "F\eta_a"] \arrow[rr, equal, "id_{F a}"] & & F a
\end{tikzcd}

\begin{tcolorbox}[breakable] 
In Haskell, this typeclass would be:

\begin{code}
class (Functor f c d, Functor g d c)
  => Adjunction f g c d where
  eta :: c x (g (f x))
  epsilon :: d (f (g y)) y
\end{code}

With rules:

\begin{itemize}
  \item \texttt{eta} is a natural transformation
  \item \texttt{epsilon} is a natural transformation
  \item | map epsilon . eta = id |
  \item | epsilon . map eta = id |
\end{itemize}
\end{tcolorbox}

\subsection{Monad}

A Monad is a functor $M \colon \mathcal{C} \rightarrow \mathcal{C}$, and two natural transformations $\eta \colon I \dotarrow M$, $\mu \colon M^2 \dotarrow M$.
Such that the following diagrams commute:

\begin{tikzcd}
  M a \arrow[r, "\eta_{M a}"] \arrow[rd, swap, equal, "id_{M a}"] & M^2 a \arrow[d, "\mu_a"] & M a \arrow[l, swap, "M \eta_a"] \arrow[ld, equal, "id_{M a}"] \\
  & M a
\end{tikzcd}
\begin{tikzcd}
 M^3 a \arrow[r, "M\mu_a"] \arrow[d, "\mu_{M a}"] & M^2 a \arrow[d, "\mu_a"] \\
 M^2 a \arrow[r, "\mu_a"] & M a
\end{tikzcd}

\begin{tcolorbox}[breakable] 
In Haskell, this typeclass would be:

\begin{code}
class (Functor m c c)
  => Monad m c where
  eta :: c x (m x)
  bind :: c a (m b) -> c (m a) (m b)
  bind f = mu . fmap f
  mu :: c (m (m x)) (m x)
  mu = bind id
\end{code}

With rules:

\begin{itemize}
  \item \texttt{eta} is a natural transformation
  \item \texttt{bind} is a natural transformation
  \item | bind eta = id |
  \item | bind f . eta = f |
  \item | bind g . bind f = bind (bind g . f) |
\end{itemize}
\end{tcolorbox}

\section{Main section}

\subsection{Kleisli category}

The Kleisli category for a monad $M \colon \mathcal{C} \rightarrow \mathcal{C}$ is the category where the objects are just objects of $\mathcal{C}$ and arrows are Kleisli arrows. The definition of Kleisli arrow is below.

This category will be denoted by $\Kleisli{M}$.

\subsubsection{Kleisli arrow}
\label{karrows}

Given a Monad $(M,\eta,\mu)$ in $\mathcal{C}$ and $a,b \in obj(\mathcal{C})$, a \textbf{Kleisli arrow} from $a$ to $b$ is an arrow $f \colon a \rightarrow M b$ in $\mathcal{C}$.

Given two Kleisli arrows $f \colon a \rightarrow M b$, $g \colon b \rightarrow M c$, the composition is defined as $g \Kleislicomp{M} f := \Kleislicompdef{M}{g}{f}{c}$, and the identity is $\eta_a$.

\subsubsection{Category proof}
\begin{lemma}
The Kleisli category is a category
\end{lemma}

\begin{proof}
It can be seen that $\eta_a$ is the identity at $a$ for $\Kleislicomp{M}$, because (invoking a monad triangle identity):
\[
\eta_b \Kleislicomp{M} f = \Kleislicompdef{M}{\eta_b}{f}{b} = 
\begin{tikzcd}
a \arrow[r,"f"] & M b \arrow[r, "M \eta_b"] \arrow[dr, equal, "id_{M b}"] & M^2 b \arrow[d, "\mu_b"] \\
& & M b
\end{tikzcd} = id_{M b} \circ f = f
\] and (invoking naturality of $\eta_b$, and the other monad triangle identity):
\[
f \Kleislicomp{M} \eta_a = \Kleislicompdef{M}{f}{\eta_a}{b} =
\begin{tikzcd}
a \arrow[r, "\eta_a"] \arrow[d,"f"] & M a \arrow[d, "M f"] \\
M b \arrow[r, "\eta_{M b}"] \arrow[dr, equal, "id_{M b}"] & M^2 b \arrow[d,"\mu_b"] \\
& M b
\end{tikzcd} = id_{M b} \circ f = f
\]

(If you have trouble seeing how the diagram is relevant: the rightmost path is $\Kleislicompdef{M}{f}{\eta_a}{b}$ and the leftmost path is $id_{Mb} \circ f$.)


It can also be seen that composition is associative, let us lay down $h \Kleislicomp{M} (g \Kleislicomp{M} f) = \Kleislicompdef{M}{h}{(\Kleislicompdef{M}{g}{f}{c})}{d} = \Kleislicompdef{M}{h}{\Kleislicompdef{M}{g}{f}{c}}{d}$ in a diagram in the following fashion:
\[
\begin{tikzcd}
a \arrow[r,"f"] & M b \arrow[r,"Mg"] & M^2 c \arrow[r, "\mu_c"] & M c \arrow[d,"Mh"] \\
                &                    &                          & M^2 d \arrow[d,"\mu_d"] \\
                &                    &                          & M d
\end{tikzcd}
\]

We now complete it with a naturality square for $\mu$:
\[
\begin{tikzcd}
a \arrow[r,"f"] & M b \arrow[r,"Mg"] & M^2 c \arrow[r, "\mu_c"] \arrow[d, "M^2h"] & M c \arrow[d,"Mh"] \\
                &                    & M^3 d \arrow[r, "\mu_{Md}"]                & M^2 d \arrow[d,"\mu_d"] \\
                &                    &                                            & M d
\end{tikzcd}
\]

And the square monad law (rotate your head 90$^\circ$ counterclockwise, if you can't match it at first):
\[
\begin{tikzcd}
a \arrow[r,"f"] & M b \arrow[r,"Mg"] & M^2 c \arrow[r, "\mu_c"] \arrow[d, "M^2h"]      & M c \arrow[d,"Mh"] \\
                &                    & M^3 d \arrow[r, "\mu_{Md}"] \arrow[d, "M\mu_d"] & M^2 d \arrow[d,"\mu_d"] \\
                &                    & M^2 d \arrow[r, "\mu_d"]                        & M d
\end{tikzcd}
\]

We get now that $h \Kleislicomp{M} (g \Kleislicomp{M} f) = \Kleislicompdef{M}{h}{\Kleislicompdef{M}{g}{f}{c}}{d} = \mu_d \circ M \mu_d \circ M^2 h \circ M g \circ f = \Kleislicompdef{M}{(\Kleislicompdef{M}{h}{g}{d})}{f}{d} = (h \Kleislicomp{M} g) \Kleislicomp{M} f$.
\end{proof}
\subsection{Main theorem}

Reproducing word by word what was written in the Eilenberg-Moore Lecture:

Given the abstract of the notes, we want to prove something like:

\begin{theoremish}
  For a monad $M$, there are two functors $F \colon \mathcal{C} \rightarrow \mathcal{?}$ and $G \colon \mathcal{?} \rightarrow \mathcal{C}$, such that $F \dashv G$ and $GF = M$.
\end{theoremish}

The domain of $F$ and codomain $G$ are given by $M$, but we still have to determine the other category in the adjunction. If you assume I did not spend a whole section defining a category just to spend some time and have some fun, you probably realized by now that $\mathcal{?} = \Kleisli{M}$.

\begin{theorem}
  For a monad $M$, there are two functors $F \colon \mathcal{C} \rightarrow \Kleisli{M}$ and $G \colon \Kleisli{M} \rightarrow \mathcal{C}$, such that $F \dashv G$ and $GF = M$.
\end{theorem}

\begin{proof}

The proof is divided in a few sections:

\subsubsection{Choosing F}

Now, what are these two functors? Well, let us start with $F$. Given $a \in \mathcal{C}$, we want to find a $F a \in \Kleisli{M}$, i.e., also an object in $\mathcal{C}$. Well, for lack of better ideas, let's just make $F a = a$.

In functions, given $f \colon a \rightarrow b$ in $\mathcal{C}$, we want to find an $F f \colon a \rightarrow b$ in $\Kleisli{M}$, i.e., an $F f \colon a \rightarrow M b$ in $\mathcal{C}$. Given that $\eta_b \colon b \rightarrow M b$, we go for $F f = \Fdef{f}{b}$.

$F$ defined like this is indeed a functor, for $F(id_a) = \Fdef{id_a}{a} = \eta_a$ which is the Kleisli identity and 
\[
F g \Kleislicomp{M} F f = \Kleislicompdef{M}{F g}{F f}{c} = \Kleislicompdef{M}{(\Fdef{g}{c})}{(\Fdef{f}{b})}{c} = \mu_c \circ M \eta_c \circ M g \circ \eta_b \circ f = \\
\]
\[
\begin{tikzcd}
a \arrow[r,"f"] & b \arrow[r, "\eta_b"] \arrow[d, "g"] & M b \arrow[d, "M g"]      \\
                & c \arrow[r, "\eta_c"]                & M c \arrow[r, "M \eta_c"] \arrow[rd, equal, "id_{Mc}"] & M^2 c \arrow [d, "\mu_c"] \\
                &                                      &                                                        & M c
\end{tikzcd}
 = id_{Mc} \circ \eta_c \circ g \circ f = \eta_c \circ {g \circ f} = F (g \circ f)
\]

where the square in the commutative diagram is the naturality of $\eta$ and the triangle is a monad identity. \blackqed

\subsubsection{Choosing G}

What about $G$?

Well, we want $M = GF$, so $G (F a) = G a = M a$. That was easy.

$G f$ needs to take an arrow $f \colon a \rightarrow M b$ to an arrow $G f \colon G a \rightarrow G b$, i.e., $f \colon M a \rightarrow M b$. This is a known function in Haskell land (|(=<<) :: (a -> M b) -> M a -> M b|), so of course there is an implementation in regular categories, which is: $G f = \Gdef{M}{f}{b}$

We know that $GF a = M a$, but what about $GF f$?

$GF f = G (F f) = G (\Fdef{f}{b}) = \Gdef{M}{(\Fdef{f}{b})}{b} = \mu_b \circ M \eta_b \circ M f = M f$ (by one of the Monad triangle identities).

And is G a functor?

Well, $G(id_a) = \Gdef{M}{\eta_a}{a} = id_{Ma}$ (again by the same triangle identity as above), and

\[
Gg \circ Gf = \Gdef{M}{g}{c} \circ \Gdef{M}{f}{b} =
\]
\[
\begin{tikzcd}
M a \arrow[r, "M f"] & M^2 b \arrow[r, "\mu_b"]  \arrow[d, "M^2 g"]  & M b   \arrow[d, "M g"] \\
                     & M^3 c \arrow[r, "\mu_Mc"] \arrow[d, "M\mu_c"] & M^2 c \arrow[d,"\mu_c"] \\
                     & M^2 c \arrow[r, "\mu_c"]                      & M c
\end{tikzcd}
\]
\[
 = \mu_c \circ M \mu_c \circ M^2 g \circ M f = \Gdef{M}{(\Kleislicompdef{M}{g}{f}{c})}{c} = G(\Kleislicompdef{M}{g}{f}{c}) = G(g \Kleislicomp{M} f)
\]

with the squares being naturality of $\mu$ and the Monad square identity.

 \blackqed

\subsubsection{Choosing $\eta$ and $\epsilon$}

Now for the adjunction unit and counit. We want $\eta \colon I \dotarrow GF = M$. As usual, our definition for $\eta$ is the same as the $\eta$ for the monad.

The counit is $\epsilon \colon FG \dotarrow I$, i.e.: $\epsilon_{a} \colon M a \rightarrow a$. But, as we all know, a monad does not allow for extracting its value, which means we give up...


... until we remember that $\epsilon_a$ is an arrow in $\Kleisli{M}$, which means it is actually $\colon M a \rightarrow M a$ in $\mathcal{C}$! And we know of just the perfect candidate for that, $id_{Ma}$.

\subsubsection{Loose ends}

Remaining to check is:

\begin{enumerate}
  \item \label{etanattrans} $\eta$ is a natural transformation;
  \item \label{epsilonnattrans} $\epsilon = id$ is a natural transformation;
  \item \label{diagrams} The identity diagrams of an adjunction commute.
\end{enumerate}

As we have been getting used to by the previous proofs, \cref{etanattrans} is trivial ($\eta$ is a natural transformation by the assumption that $M$ is a monad). \blackqed

\Cref{epsilonnattrans} is saying that the following diagram commmutes in $\Kleisli{M}$:

\[
\begin{tikzcd}
FG a = M a \arrow[r, "id_{Ma}"] \arrow[d, "FG f = \Fdef{(\Gdef{M}{f}{b})}{Mb}"] &[20pt] a \arrow[d,"f"] \\
FG b = M b \arrow[r, "id_{Mb}"]                     & b
\end{tikzcd}
\]

Algebraically: $f \Kleislicomp{M} id_{Ma} = id_{Mb} \Kleislicomp{M} (\Fdef{(\Gdef{M}{f}{b})}{Mb})$.

To prove this, expand the right-hand side:
\begin{align}
      id_{Mb} \Kleislicomp{M} (\Fdef{(\Gdef{M}{f}{b})}{Mb}) =
   \\ \Kleislicompdef{M}{id_{Mb}}{(\Fdef{(\Gdef{M}{f}{b})}{Mb})}{b} = && & \text{\footnotesize{(by definition)}}
   \\ \mu_b \circ \eta_{Mb} \circ \mu_b \circ M f =                   && & \text{\footnotesize{(by associativity of $\circ$)}}
   \\ \mu_b \circ M f =                                               && & \text{\footnotesize{(by one of the monad triangle identities)}}
   \\ \Kleislicompdef{M}{f}{id_{Ma}}{b} =                             && & \text{\footnotesize{(by identity on the right)}}
   \\ f \Kleislicomp{M} id_{Ma}                                       && & \text{\footnotesize{(by definition)}}
\end{align}
\blackqed

Now we come to the last part. \Cref{diagrams} is proving that the following commute:

\[
\begin{tikzcd}
                                                        & GFG a \arrow[d, "G\epsilon_a"] &                                                        & FGF a \arrow[d,"\epsilon_{Fa}"] \\
G a \arrow[ru, "\eta_{Ga}"] \arrow[r, equal, "id_{Ga}"] & G a                            & F a \arrow[ru, "F\eta_a"] \arrow[r, equal, "id_{F a}"] & F a
\end{tikzcd}
\]

replacing with our definitions:

\[
\begin{tikzcd}
                                                        & M^2 a \arrow[d, "\Gdef{M}{id_{Ma}}{a}"] &[20pt]                                                              & M a \arrow[d,"id_{Ma}"] \\
M a \arrow[ru, "\eta_{Ma}"] \arrow[r, equal, "id_{Ma}"] & M   a                                   &       a \arrow[ru, "\Fdef{\eta_a}{Ma}"] \arrow[r, equal, "\eta_a"] & a
\end{tikzcd}
\]

The first diagram is just one of the triangle identity for monads.

For the second diagram, don't forget that the diagram is in the $\Kleisli{M}$ category (which is why $\eta_a$ is written instead of $id_a$), which means the diagram is stating $id_{Ma} \Kleislicomp{M} (\eta_{M a} \circ \eta_a) = \Kleislicompdef{M}{id_{Ma}}{\eta_{Ma} \circ \eta_a}{a} = \mu_a \circ \eta_{Ma} \circ \eta_a = \eta_a $ by the same triangle identity for monads.

This ends our proof.

\end{proof}

\end{document}

