\documentclass{article}
%include polycode.fmt
\usepackage{tikz-cd}
\usepackage{graphicx}
\usepackage{amsmath, amssymb}
\usepackage{amsthm,thmtools}
\usepackage{hyperref, cleveref}
\usepackage[most]{tcolorbox}
\usepackage[utf8]{inputenc}
\newcommand{\dotarrow}{% to be used in math mode...
	\mathrel{\ooalign{\hss\raise.65ex\hbox{\scalebox{1.25}{\normalfont .}}%
	\kern0.35ex\hss\cr$\longrightarrow$}}}
		
\declaretheorem[name=Lemma]{lemma}
\declaretheorem[name=Theorem]{theorem}
\declaretheorem[name=Underspecified theorem]{theoremish}

\declaretheoremstyle[postheadspace=\newline]{def}
\declaretheorem[style=def,name=Definition]{definition}

%opening
\title{Adjunctions and Monads \\
      \large A Love Story -- Part II}
\author{Cristóvão Gomes Ferreira}

\begin{document}

\newcommand{\blackqed}{
	\renewcommand{\qedsymbol}{$\blacksquare$}
	\qed
	\renewcommand{\qedsymbol}{$\square$}
}

\newcommand{\ignore}[1]{}
\newcommand\mdoubleplus{\mathbin{+\mkern-10mu+}}

\newcommand{\catname}[1]{{\normalfont\textbf{#1}}}
\newcommand{\algebra}[3]{\begin{tikzcd}#1 #2 \arrow[d,"#3"] \\ #2\end{tikzcd}}
\newcommand{\algebralike}[3]{\begin{tikzcd}#1 \arrow[d,"#3"] \\ #2\end{tikzcd}}

\maketitle

\begin{abstract}
We are going to prove that given a monad $M$, we can define an adjunction $F \dashv G$, such that $GF = M$, continuing our Monad and Adjunction love story!
\end{abstract}

\section{Definitions}

\subsection{Prelude}

\ignore {
\begin{code}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleInstances #-}
module EilenbergMoore where
import CategoryPrelude
import Data.Foldable (Foldable(..))
\end{code}
}

For these notes, we define the following conventions:

\begin{itemize}
	\item $\mathcal{C}$, $\mathcal{D}$ or any calligraphed letter is a variable denoting a category.
  \item $\catname{Hask}, \catname{Set}$ or any bold text denotes a category.
	\item $Hom(x,y)$ is the set of arrows between objects $x$ and $y$.
	\item $I$ is the identity functor ($Ic=c$, $If=f$).
	\item Basic category theory identities are trivial, i.e., will not be invoked but just used.
\end{itemize}

\subsection{Adjunction}

Let $F$ and $G$ such that:

\begin{tikzcd}
  \mathcal{C} \arrow[r, shift left, "F"] & \mathcal{D} \arrow[l, shift left, "G"]
\end{tikzcd}

We say that $F$ is left adjoint to $G$, written $F \dashv G$, when some rules apply.

There are several equivalent definitions of the rules in this relation, in these notes we are going to use the following:
	
$F \dashv G$ if there are two natural transformations
$\eta \colon I \dotarrow GF$ and $\epsilon \colon FG \dotarrow I$ such that the following diagrams commute:

\begin{tikzcd}
 & G F G a \arrow[rd, "G\epsilon_a"] \\
 G a \arrow[ru, "\eta_{G a}"] \arrow[rr, equal,"id_{G a}"] & & G a
\end{tikzcd}
\begin{tikzcd}
 & F G F a \arrow[rd, "\epsilon_{F a}"] \\
 F a \arrow[ru, "F\eta_a"] \arrow[rr, equal, "id_{F a}"] & & F a
\end{tikzcd}

\begin{tcolorbox}[breakable] 
In Haskell, this typeclass would be:

\begin{code}
class (Functor f c d, Functor g d c)
  => Adjunction f g c d where
  eta :: c x (g (f x))
  epsilon :: d (f (g y)) y
\end{code}

With rules:

\begin{itemize}
  \item \texttt{eta} is a natural transformation
  \item \texttt{epsilon} is a natural transformation
  \item | map epsilon . eta = id |
  \item | epsilon . map eta = id |
\end{itemize}
\end{tcolorbox}

\subsection{Monad}

A Monad is a functor $M \colon \mathcal{C} \rightarrow \mathcal{C}$, and two natural transformations $\eta \colon I \dotarrow M$, $\mu \colon M^2 \dotarrow M$.
Such that the following diagrams commute:

\begin{tikzcd}
  M a \arrow[r, "\eta_{M a}"] \arrow[rd, swap, equal, "id_{M a}"] & M^2 a \arrow[d, "\mu_a"] & M a \arrow[l, swap, "M \eta_a"] \arrow[ld, equal, "id_{M a}"] \\
  & M a
\end{tikzcd}
\begin{tikzcd}
 M^3 a \arrow[r, "M\mu_a"] \arrow[d, "\mu_{M a}"] & M^2 a \arrow[d, "\mu_a"] \\
 M^2 a \arrow[r, "\mu_a"] & M a
\end{tikzcd}

\begin{tcolorbox}[breakable] 
In Haskell, this typeclass would be:

\begin{code}
class (Functor m c c)
  => Monad m c where
  eta :: c x (m x)
  bind :: c a (m b) -> c (m a) (m b)
  bind f = mu . fmap f
  mu :: c (m (m x)) (m x)
  mu = bind id
\end{code}

With rules:

\begin{itemize}
  \item \texttt{eta} is a natural transformation
  \item \texttt{bind} is a natural transformation
  \item | bind eta = id |
  \item | bind f . eta = f |
  \item | bind g . bind f = bind (bind g . f) |
\end{itemize}
\end{tcolorbox}

\section{Main section}

\subsection{Eilenberg-Moore category}

The Eilenberg-Moore category for a monad $M$ is the category where the objects are monadic $M$-algebras and the arrows are $M$-algebra morphisms. These are both defined below.

This category will be denoted by $\catname{M-Alg}$.

\subsubsection{Monad algebras}
\label{malgebras}

Given a Monad $(M,\eta,\mu)$ in $\mathcal{C}$, a \textbf{(monadic) }$\mathbf{M}$\textbf{-algebra} is a pair $(a, \theta)$ where $a \in obj(\mathcal{C})$ and $\theta \colon M a \rightarrow a$, such that the following diagrams commute.

\[
\begin{tikzcd}
& M a \arrow[d, "\theta"] \\
a \arrow[ru, "\eta_a"] \arrow[r, equal, "id"] & a
\end{tikzcd}
\begin{tikzcd}
M^2 a \arrow[r, "M\theta"] \arrow[d,"\mu_a"] & M a \arrow[d,"\theta"] \\
M a \arrow[r, "\theta"] & a
\end{tikzcd}
\]

The definition of an algebra for a Endofunctor is the same, but without the diagrams. We will drop the monadic adjective for the remainder of this text, for there is no risk of confusion: we are always talking about this kind of algebra.
The algebra for an endofunctor will be called an endofunctor algebra.

We represent a pair $(a,\theta)$ with the diagram $\algebra{M}{a}{\theta}$, this also works for endofunctor algebras.

\subsubsection{Algebra morphisms}
Given two (monadic or endofunctor) $F$-algebras $\algebra{F}{a}{\theta_1} ,\algebra{F}{b}{\theta_2}$ for $F$ in $\mathcal{C}$, an $\mathbf{F}$\textbf{-algebra morphism} from $\algebra{F}{a}{\theta_1}$ to $\algebra{F}{b}{\theta_2}$ is an arrow $f \colon a \rightarrow b$ such that the following diagram commutes:
\[
\begin{tikzcd}
F a \arrow[r, "F f"] \arrow[d, "\theta_1"] & F b \arrow[d, "\theta_2"] \\
  a \arrow[r, "f"]                         &   b
\end{tikzcd}
\]

The identity $id_a$ is an algebra morphism, and the composition of algebra morphisms is an algebra morphism (the proof of these facts is left as an exercise to the reader). This means that we can define the morphisms as arrows for $\catname{M-Alg}$.

\subsection{Main theorem}

Given the abstract of the notes, we want to prove something like:

\begin{theoremish}
  For a monad $M$, there are two functors $F \colon \mathcal{C} \rightarrow \mathcal{?}$ and $G \colon \mathcal{?} \rightarrow \mathcal{C}$, such that $F \dashv G$ and $GF = M$.
\end{theoremish}

The domain of $F$ and codomain $G$ are given by $M$, but we still have to determine the other category in the adjunction. If you assume I did not spend a whole section defining a category just to spend some time and have some fun, you probably realized by now that $\mathcal{?} = \catname{M-Alg}$.

\begin{theorem}
  For a monad $M$, there are two functors $F \colon \mathcal{C} \rightarrow \catname{M-Alg}$ and $G \colon \catname{M-Alg} \rightarrow \mathcal{C}$, such that $F \dashv G$ and $GF = M$.
\end{theorem}

\begin{proof}

The proof is divided in a few sections:

\subsubsection{Choosing F}

Now, what are these two functors? Well, let us start with $F$. Given $a \in \mathcal{C}$, we want to find a $F a \in \catname{M-Alg}$, i.e., something of the sort $\algebra{M}{x}{\theta}$, with $x$ related to $a$ in some way.
With some perusing of the available options, it becomes clear that a good candidate for $F a$ is
\[
F a = \algebralike{M (M a)}{M a}{\mu_a} = \algebralike{M^2 a}{M a}{\mu_a}
\]

In functions, given $f \colon a \rightarrow b$, we want to find $F f \colon \algebralike{M^2 a}{M a}{\mu_a} \rightarrow \algebralike{M^2 b}{M b}{\mu_b}$, i.e. a $F f \colon M a \rightarrow_{\mathcal{C}} M b$ such that this commutes:
\[
\begin{tikzcd}
M^2 a \arrow[r, "M (F f)"] \arrow[d, "\mu_a"] & M^2 b \arrow[d, "\mu_b"] \\
M   a \arrow[r, "   F f "]                    & M   b
\end{tikzcd}
\]

If we set $F f = M f$, the diagram commutes by naturality of $\mu$, so we set that and we're done.

\subsubsection{Choosing G}

What about $G$? Well, we want $M = GF$, so $G (F a) = G (\algebralike{M^2 a}{M a}{\mu_a}) = M a$. So a reasonable and simple choice seems to be $G (\algebra{M}{a}{\theta}) = a$ (or, in the pair notation $G(a, \theta)=a$).

$G f$ is quite straightforward: An arrow in \catname{M-Alg} is just a normal arrow in $\catname{C}$ between the underlying objects of the algebras. Give that $G$ in objects is precisely taking the underlying object, $G$ in arrows is just the arrow ($G$ is a forgetful functor!).

\subsubsection{Choosing $\eta$ and $\epsilon$}

Now for the adjunction unit and counit. We want $\eta \colon I \dotarrow GF = M$. As usual, our definition for $\eta$ is the same as the $\eta$ for the monad.

The counit is $\epsilon \colon FG \dotarrow I$, i.e.: $\epsilon_{(a,\theta)} \colon \algebralike{M^2 a}{M a}{\mu} \rightarrow \algebra{M}{a}{\theta}$, which means we have to find an arrow $\epsilon_{(a,\theta)} \colon M a \rightarrow a$ and then check that it is an $M$-Algebra morphism.

The only arrow with that domain and codomain we have is precisely $\theta$, so $\epsilon_{(a,\theta)} = \theta$ (which is somehow fitting: $G$ is |fst|, $\epsilon$ is |snd|).

\subsubsection{Loose ends}

Remaining to check is:

\begin{enumerate}
  \item \label{etanattrans} $\eta$ is a natural transformation;
  \item \label{epsilonarrow} $\epsilon_{(a,\theta)} = \theta$ is an $M$-algebra;
  \item \label{epsilonnattrans} $\epsilon = |snd|$ is a natural transformation;
  \item \label{diagrams} The identity diagrams of an adjunction commute.
\end{enumerate}

As we have been getting used to by the previous proofs, \cref{etanattrans} is trivial.

\Cref{epsilonarrow} is saying that the following diagram commmutes:

\[
\begin{tikzcd}
M^2 a \arrow[r, "M\theta"] \arrow[d, "\mu"] & M a \arrow[d,"\theta"] \\
M   a \arrow[r, "\theta"]                   & a
\end{tikzcd}
\]

but this is the second diagram for $M$-algebras defined in \cref{malgebras}, so we're done.

\Cref{epsilonnattrans} is proving the following diagram (and some more implicit arrows, which commutation was already proved or is trivial) commutes:

\[
\begin{tikzcd}
M^2 a \arrow[rd, swap, "\mu_a"] &[-25pt]                                          &[10pt]                 &[-25pt] M a \arrow[ld,       "\theta_1"] \\[-10pt]
                                &        M a \arrow[r,"\theta_1"] \arrow[d,"M f"] &       a \arrow[d,"f"] &                                         \\[10pt]
                                &        M b \arrow[r,"\theta_2"]                 &       b               &                                         \\[-10pt]
M^2 b \arrow[ru,       "\mu_a"] &                                                 &                       &        M b \arrow[lu, swap, "\theta_2"]
\end{tikzcd}
\]

This diagram commutes because the arrows are arrows in \catname{M-Alg}, i.e., $f$ is an $M$-algebra morphism, and this is precisely the law that an $M$-algebra morphism must satisfy.

Now we come to the last part. \Cref{diagrams} is proving that the following commute:

\[
\begin{tikzcd}
& GFG (a, \theta) \arrow[d, "G\epsilon_a"] & & FGF a \arrow[d,"\epsilon_{Fa}"] \\
G(a,\theta) \arrow[ru, "\eta_{Ga}"] \arrow[r, equal, "id"] & G (a, \theta) & F a \arrow[ru, "F\eta_a"] \arrow[r, equal, "id"] & F a
\end{tikzcd}
\]

replacing for our definition:

\[
\begin{tikzcd}
& M a \arrow[d, "\theta"] & & (M^2 a, \mu_{Ma}) \arrow[d,"\mu_a"] \\
a \arrow[ru, "\eta_{Ga}"] \arrow[r, equal, "id"] & a & (M a, \mu_a) \arrow[ru, "M\eta_a"] \arrow[r, equal, "id"] & (M a, \mu_a)
\end{tikzcd}
\]

The first diagram is just the first diagram for monoidal $M$-algebras defined in \cref{malgebras}, and the second diagram is an identity for monads.

This ends our proof.

\end{proof}

\end{document}


