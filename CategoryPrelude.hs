{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ConstraintKinds #-}
module CategoryPrelude (
  Category(..),
  Functor(..),
  Homo(..),
  Forget(..),
  P.Bool(..),
  P.Eq(..),
  Proxy.Proxy(..),
  P.Monoid(..)) where
import qualified Prelude as P
import Prelude (Monoid(..))
import GHC.Exts (Constraint)
import Data.Proxy as Proxy
import Data.List as List

infixr 9 .
class NoConstraint a
instance NoConstraint a

class Category c where
  type Object c :: * -> Constraint
  id :: Object c x => c x x
  (.) :: c y z -> c x y -> c x z

class (Category c, Category d) => Functor f c d | f -> c d where
  map :: c x y -> d (f x) (f y)

instance Category (->) where
  type Object (->) = NoConstraint
  id a = a
  (.) f g a = f (g a)

data Forget cstr m where
  Forgot :: cstr m => m -> Forget cstr m

data Homo cstr a b where
  AssertHomo :: (cstr a, cstr b) => (a -> b) -> Homo cstr a b

instance Category (Homo cstr) where
  type Object (Homo cstr) = cstr
  id = AssertHomo id
  AssertHomo f . AssertHomo g  = AssertHomo (f . g)

instance Functor (Forget cstr) (Homo cstr) (->) where
  map (AssertHomo f) (Forgot a) = Forgot (f a)

instance Functor [] (->) (Homo Monoid) where
  map f = AssertHomo (List.map f)

