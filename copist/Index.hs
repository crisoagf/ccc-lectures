{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
module Index (generateIndex) where
import Lucid
import qualified Data.Text as T

generateIndex :: FilePath -> [(String, FilePath)] -> IO ()
generateIndex whereTo files = renderToFile whereTo (indexHtml files)

indexHtml :: [(String, FilePath)] -> Html ()
indexHtml files = 
  doctypehtml_ $ do
    head_ $ do
      meta_ [charset_ "utf-8"]
      meta_ [name_ "description", content_ "Index of the notes for CCC"]
      meta_ [name_ "author", content_ "Cristóvão Gomes Ferreira"]
      title_ "Notes index"
      link_ [rel_ "stylesheet", href_ "style.css"]
    body_ $ do
      h1_ "Index of the notes"
      foldr (\ (title, fp) -> (p_ (a_ [href_ fp] (toHtml title)) >>)) (pure ()) (fmap T.pack <$> files)
