module Main where
import Development.Shake
import Development.Shake.Command
import Development.Shake.FilePath
import Development.Shake.Util
import Index

main :: IO ()
main = shakeArgs shakeOptions{shakeFiles="build"} $ do
  action $ do
    srcs <- getDirectoryFiles "." ["*.lhs"]
    need $ fmap (("build" </>) . (-<.> "pdf")) srcs
    need [ "build" </> "index.html" ]
  
  phony "clean" $ do
    putInfo "Cleaning files in build"
    removeFilesAfter "_build" ["//*"]

  "build" </> "*.pdf" %> \out -> do
    let texfile = out -<.> "tex"
    need [texfile]
    cmd_ "pdflatex" ("-output-directory=build") [texfile]
    cmd_ "pdflatex" ("-output-directory=build") [texfile]

  "build" </> "*.tex" %> \out -> do
    let lhs = dropDirectory1 $ out -<.> "lhs"
    need [lhs]
    cmd_ "lhs2TeX" [lhs] "-o" [out]
  
  "build" </> "index.html" %> \out -> do
    need ["build" </> "style.css"]
    pdfs <- fmap (-<.> "pdf") <$> getDirectoryFiles "" ["*.lhs"]
    liftIO $ generateIndex out ((\ file -> (dropExtension file, file)) <$> pdfs)
  
  "build" </> "style.css" %> copyFile' ("static" </> "style.css")
